package com.cliption.cherry.singeltons;

import com.cliption.cherry.models.config.ConfigPath;
import com.cliption.cherry.models.config.DynamicConfigPath;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class Config {

    private static Config _instance;
    private File _file;
    private YamlConfiguration _config;

    public static synchronized Config getInstance() {
        if (_instance == null) {
            _instance = new Config();
        }
        return _instance;
    }

    public Config() {
        File dir = new File("./plugins/Cherry/");
        if (!dir.exists()) {
            dir.mkdirs();
        }

        _file = new File(dir, "config.yml");
        if (!_file.exists()) {
            try {
                _file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        _config = YamlConfiguration.loadConfiguration(_file);
    }

    public boolean containsPath(DynamicConfigPath inPath) {
        return _config.contains(inPath.getPath());
    }

    public boolean containsPath(ConfigPath inPath) {
        return _config.contains(inPath.getPath());
    }

    public void addConfigValue(DynamicConfigPath inPath, String inNewValue) {
        Object retValues = getOrCreateConfigValue(inPath, "");
        if (retValues != null && !retValues.toString().isEmpty()) {
            retValues = inNewValue + ";";
            setConfigValue(inPath, retValues);
        } else {
            String firstName = inNewValue + ";";
            setConfigValue(inPath, firstName);
        }
    }

    public void setConfigValue(DynamicConfigPath inPath, Object inObject) {
        _config.set(inPath.getPath(), inObject);
    }

    public void setConfigValue(ConfigPath inPath, Object inObject) {
        _config.set(inPath.getPath(), inObject);
    }

    public Object getConfigValue(DynamicConfigPath inPath) {
        if (!containsPath(inPath)) {
            return null;
        }
        return _config.get(inPath.getPath());
    }

    public Object getConfigValue(ConfigPath inPath) {
        if (!containsPath(inPath)) {
            return null;
        }
        return _config.get(inPath.getPath());
    }

    public Object getOrCreateConfigValue(DynamicConfigPath inPath, Object inDefaultValue) {
        Object configValue = getConfigValue(inPath);
        if (configValue == null) {
            setConfigValue(inPath, inDefaultValue);
            return inDefaultValue;
        }
        return configValue.toString();
    }

    public Object getOrCreateConfigValue(ConfigPath inPath, Object inDefaultValue) {
        Object configValue = getConfigValue(inPath);
        if (configValue == null) {
            setConfigValue(inPath, inDefaultValue);
            return inDefaultValue;
        }
        return configValue;
    }
}
