package com.cliption.cherry.singeltons;

import com.cliption.cherry.models.items.ItemWhip;

public class ItemManager {
    private static ItemManager _instance;

    // Insert List of Items and abstract Item Class for better usability
    private ItemWhip whip;

    public static synchronized ItemManager getInstance() {
        if (_instance == null) {
            _instance = new ItemManager();
        }
        return _instance;
    }

    public ItemManager() {
        initAllItems();
    }

    private void initAllItems() {
        whip = new ItemWhip();
    }

    public ItemWhip getWhip() {
        return whip;
    }
}
