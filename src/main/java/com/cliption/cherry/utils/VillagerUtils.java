package com.cliption.cherry.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Villager;

import java.util.ArrayList;
import java.util.List;

public class VillagerUtils {
    public static final String ownerPrefix = "Owner:";

    public static List<Villager> getVillagers() {
        List<Villager> villagers = new ArrayList<>();
        for (World world : Bukkit.getWorlds()) {
            for (LivingEntity entity : world.getLivingEntities()) {
                if (entity instanceof Villager) {
                    Villager villager = (Villager) entity;
                    villagers.add(villager);
                }
            }
        }
        return villagers;
    }

    public static String formatInitialVillagerName(int slaveCounter, String playerName) {
        return String.format("%s %s %s", ChatColor.RED + String.valueOf(slaveCounter), ownerPrefix, ChatColor.GREEN + playerName);
    }

    public static String formatVillagerName(String name, String ownerName) {
        return String.format("%s %s %s", ChatColor.RED + name, ownerPrefix, ChatColor.GREEN + ownerName);
    }

    public static boolean isVillagerOwnedByPlayer(Villager villager, String playerName) {
        if (villager.getCustomName() != null && !villager.getCustomName().trim().isEmpty()) {
            if (villager.getCustomName().contains(ownerPrefix) && playerName != null && !playerName.trim().isEmpty()) {
                return villager.getCustomName().substring(villager.getCustomName().indexOf(ownerPrefix) + ownerPrefix.length()).trim().equalsIgnoreCase(playerName);
            }
        }
        return false;
    }

    public static String getCurrentOwnerOfVillager(Villager villager) {
        if (villager.getCustomName() != null && !villager.getCustomName().trim().isEmpty()) {
            if (villager.getCustomName().contains(ownerPrefix)) {
                return villager.getCustomName().substring(villager.getCustomName().indexOf(ownerPrefix) + ownerPrefix.length()).trim();
            }
        }
        return null;
    }

    public static boolean areVillagerNamesMatching(String checkName1, String checkName2) {
        return checkName1.equalsIgnoreCase(checkName2.substring(0, checkName2.indexOf(ownerPrefix)).trim());
    }
}
