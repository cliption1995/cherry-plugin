package com.cliption.cherry.commands;

import com.cliption.cherry.models.items.ItemWhip;
import com.cliption.cherry.singeltons.ItemManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SlaveryCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;
            ItemWhip whip = ItemManager.getInstance().getWhip();
            player.getInventory().addItem(whip.getItemStack());
            player.sendMessage(ChatColor.WHITE + "Der " + ChatColor.BLACK + whip.getName() + " " + ChatColor.WHITE + "wurde dem Inventar hinzugefügt");
        }
        return false;
    }
}
