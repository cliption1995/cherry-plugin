package com.cliption.cherry.commands;

import com.cliption.cherry.Main;
import com.cliption.cherry.utils.VillagerUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

public class CallCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            if (strings != null && strings.length > 0) {
                for (String string : strings) {
                    commandSender.sendMessage("Str found: " + string);
                }

                String slaveName = strings[0];
                if (slaveName != null && !slaveName.isEmpty() && !slaveName.trim().isEmpty()) {
                    Bukkit.getScheduler().runTaskAsynchronously(Main._instance, () -> {
                        // Use VillagerUtils.getVillagers() for better code overview
                        for (World world : Bukkit.getWorlds()) {
                            for (LivingEntity entity : world.getLivingEntities()) {
                                if (entity instanceof Villager) {
                                    Villager villager = (Villager) entity;
                                    if (villager.getCustomName() != null && !villager.getCustomName().trim().isEmpty()) {
                                        if (villager.getCustomName().contains(commandSender.getName())) {
                                            if (VillagerUtils.isVillagerOwnedByPlayer(villager, commandSender.getName())) {
                                                Player player = (Player) commandSender;
                                                villager.teleport(player.getLocation());
                                                player.sendMessage("Der Sklave: " + villager.getCustomName() + " wurde zu dir teleportiert");
                                                return;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    });
                }
            } else {
                commandSender.sendMessage(ChatColor.RED + "No Slave name has been provided");
            }
        }
        return false;
    }
}
