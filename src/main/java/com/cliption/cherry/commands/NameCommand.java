package com.cliption.cherry.commands;

import com.cliption.cherry.Main;
import com.cliption.cherry.utils.VillagerUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

public class NameCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            if (strings != null && strings.length > 0) {
                Bukkit.getScheduler().runTaskAsynchronously(Main._instance, () -> {
                    String oldSlaveName = "";
                    StringBuffer newSlaveName = new StringBuffer();
                    for (int i = 0; i < strings.length; i++) {
                        if (i == 0) {
                            oldSlaveName = strings[i];
                        } else {
                            newSlaveName.append(strings[i]);
                        }
                    }
                    if (!oldSlaveName.trim().isEmpty()) {
                        if (!newSlaveName.toString().trim().isEmpty()) {
                            // Use VillagerUtils.getVillagers() for better code overview
                            for (World world : Bukkit.getWorlds()) {
                                for (LivingEntity entity : world.getLivingEntities()) {
                                    if (entity instanceof Villager) {
                                        Villager villager = (Villager) entity;
                                        if (villager.getCustomName() != null && !villager.getCustomName().trim().isEmpty()) {
                                            if (VillagerUtils.areVillagerNamesMatching(oldSlaveName, villager.getCustomName())) {
                                                villager.setCustomName(VillagerUtils.formatVillagerName(newSlaveName.toString(), commandSender.getName()));
                                                return;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }
        return false;
    }
}
