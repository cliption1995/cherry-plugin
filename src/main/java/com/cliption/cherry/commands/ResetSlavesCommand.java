package com.cliption.cherry.commands;

import com.cliption.cherry.Main;
import com.cliption.cherry.models.config.DynamicConfigPath;
import com.cliption.cherry.singeltons.Config;
import com.cliption.cherry.utils.VillagerUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

public class ResetSlavesCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            // VillagerUtils.getVillagers() benutzen (Wegen Referenzen muss geschaut werden)
            Bukkit.getScheduler().runTaskAsynchronously(Main._instance, () -> {
                // Use VillagerUtils.getVillagers() for better code overview
                for (World world : Bukkit.getWorlds()) {
                    for (LivingEntity entity : world.getLivingEntities()) {
                        if (entity instanceof Villager) {
                            Villager villager = (Villager) entity;
                            String currentOwnerOfVillager = VillagerUtils.getCurrentOwnerOfVillager(villager);
                            if (currentOwnerOfVillager != null) {
                                DynamicConfigPath slavesOwned = DynamicConfigPath.SLAVES_OWNED;
                                slavesOwned.setOwnerName(currentOwnerOfVillager);
                                try {
                                    Config.getInstance().setConfigValue(slavesOwned, 0);
                                } catch (Exception ignored) {
                                }
                            }
                            villager.setCustomNameVisible(false);
                            villager.setCustomName(null);
                        }
                    }
                }
            });
        }
        return false;
    }
}
