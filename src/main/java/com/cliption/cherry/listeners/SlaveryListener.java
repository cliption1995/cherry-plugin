package com.cliption.cherry.listeners;

import com.cliption.cherry.models.config.DynamicConfigPath;
import com.cliption.cherry.models.items.ItemWhip;
import com.cliption.cherry.singeltons.Config;
import com.cliption.cherry.singeltons.ItemManager;
import com.cliption.cherry.utils.VillagerUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Objects;

public class SlaveryListener implements Listener {
    @EventHandler
    public static void onRightClick(PlayerInteractEntityEvent event) {
        ItemStack itemInHand = event.getPlayer().getItemInHand();
        ItemWhip whip = ItemManager.getInstance().getWhip();
        if (Objects.requireNonNull(whip.getItemStack().getItemMeta()).equals(itemInHand.getItemMeta())) {
            if (event.getRightClicked().getType() == EntityType.VILLAGER) {
                event.getPlayer().sendMessage("Villager has been Right-Clicked with " + ChatColor.BLACK + whip.getName());
                String playerName = event.getPlayer().getName();
                Villager villager = (Villager) event.getRightClicked();

                String currentOwnerOfVillager = VillagerUtils.getCurrentOwnerOfVillager(villager);
                if (currentOwnerOfVillager != null) {
                    // Villager is already assigned
                    event.getPlayer().sendMessage("This Villager is already assigned to: " + currentOwnerOfVillager);
                    return;
                } else {
                    event.getPlayer().sendMessage("Villager is not assigned yet! It will be yours!");
                    DynamicConfigPath slavesOwnedConfig = DynamicConfigPath.SLAVES_OWNED;
                    slavesOwnedConfig.setOwnerName(playerName);
                    Object slavesOwnedCounter = Config.getInstance().getOrCreateConfigValue(slavesOwnedConfig, 1);
                    int slaveCounter = 1;
                    try {
                        slaveCounter = Integer.parseInt(slavesOwnedCounter.toString());
                        slaveCounter++;

                        villager.setCustomName(VillagerUtils.formatInitialVillagerName(slaveCounter, playerName));
                        villager.setCustomNameVisible(false);
                        villager.setCanPickupItems(true);
                        villager.setHealth(5);
                        villager.setVillagerType(Villager.Type.DESERT);

                        Config.getInstance().setConfigValue(slavesOwnedConfig, slaveCounter);
                        event.getPlayer().sendMessage("The Selected Villager is now Yours! Villager Name: " + villager.getCustomName());
                        villager.setProfession(Villager.Profession.SHEPHERD);
                    } catch (Exception ex) {
                        event.getPlayer().sendMessage(ChatColor.RED + "I'm not able to see the current Slave counter within Config File");
                    }
                }
            }
        }
    }
}
