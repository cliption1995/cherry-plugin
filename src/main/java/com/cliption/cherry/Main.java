package com.cliption.cherry;

import com.cliption.cherry.commands.CallCommand;
import com.cliption.cherry.commands.NameCommand;
import com.cliption.cherry.commands.ResetSlavesCommand;
import com.cliption.cherry.commands.SlaveryCommand;
import com.cliption.cherry.listeners.SlaveryListener;
import com.cliption.cherry.singeltons.ItemManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static Main _instance;

    @Override
    public void onEnable() {
        // Plugin startup logic
        Bukkit.getLogger().fine("Cherry Plugin wird aktiviert");
        // Init All additional Item Receipe's
        Bukkit.getServer().addRecipe(ItemManager.getInstance().getWhip().getReceipe());
        Bukkit.getServer().addRecipe(ItemManager.getInstance().getWhip().getShapelessRecipe());
        Bukkit.getServer().addRecipe(ItemManager.getInstance().getWhip().getFurnaceRecipe());
        // Start Listeners
        listenerRegistration();
        // Register Commands
        registerCommands();
        // Store instance to use Async-Tasks in other Classes
        _instance = this;
    }

    private void registerCommands() {
        getCommand("name").setExecutor(new NameCommand());
        getCommand("call").setExecutor(new CallCommand());
        getCommand("slavery").setExecutor(new SlaveryCommand());
        getCommand("resetlaves").setExecutor(new ResetSlavesCommand());
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        Bukkit.getLogger().fine("Cherry Plugin wird deaktiviert");
    }

    private void listenerRegistration() {
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new SlaveryListener(), this);
    }

}
