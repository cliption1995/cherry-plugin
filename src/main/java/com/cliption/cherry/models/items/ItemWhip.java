package com.cliption.cherry.models.items;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.*;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

// extend from abstract Item class
public class ItemWhip {
    private final String name = "Sklaven macher 9000";
    private ItemStack whip;
    private ShapedRecipe receipe;
    private ShapelessRecipe shapelessRecipe;
    private FurnaceRecipe furnaceRecipe;

    public ItemWhip() {
        whip = new ItemStack(Material.WOODEN_SWORD, 1);
        whip.setItemMeta(getAndChangeItemMeta());
        receipe = createShapedReceipe();
        shapelessRecipe = createShapelessRecipe();
        furnaceRecipe = createFurnaceRecipe();
    }

    private FurnaceRecipe createFurnaceRecipe() {
        FurnaceRecipe furnaceRecipe = new FurnaceRecipe(NamespacedKey.minecraft("slavewhip_smelt"), whip, Material.BLACK_WOOL, 1.0f, 8 * 20);
        return furnaceRecipe;
    }

    private ShapelessRecipe createShapelessRecipe() {
        ShapelessRecipe shapelessRecipe = new ShapelessRecipe(NamespacedKey.minecraft("slavewhip_shapeless"), whip);
        shapelessRecipe.addIngredient(2, Material.STICK);
        shapelessRecipe.addIngredient(1, Material.BLACK_WOOL);
        return shapelessRecipe;
    }

    private ShapedRecipe createShapedReceipe() {
        ShapedRecipe sr = new ShapedRecipe(NamespacedKey.minecraft("slavewhip"), whip);
        sr.shape(
                " SS",
                "SS ",
                " S "
        );
        sr.setIngredient('S', Material.STICK);
        return sr;
    }

    private ItemMeta getAndChangeItemMeta() {
        ItemMeta meta = whip.getItemMeta();
        meta.setDisplayName("Sklaven macher 9000");
        meta.setLore(getLore());
        meta.addEnchant(Enchantment.DURABILITY, 3, false);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        return meta;
    }

    private List<String> getLore() {
        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.BLACK + "This Item enslaves unmarked Villagers");
        lore.add(ChatColor.BLACK + "which have been hit with it");
        lore.add(ChatColor.RED + "Already marked Slaves will not change it's owner without permission");
        return lore;
    }

    public ShapelessRecipe getShapelessRecipe() {
        return shapelessRecipe;
    }

    public FurnaceRecipe getFurnaceRecipe() {
        return furnaceRecipe;
    }

    public ShapedRecipe getReceipe() {
        return receipe;
    }

    public ItemStack getItemStack() {
        return whip;
    }

    public String getName() {
        return name;
    }
}
