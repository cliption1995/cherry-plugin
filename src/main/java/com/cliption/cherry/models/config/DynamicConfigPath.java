package com.cliption.cherry.models.config;

public enum DynamicConfigPath {
    SLAVES_OWNED("commands.slaves.");

    private String _basePath;
    private String _ownerName;

    DynamicConfigPath(String _basePath) {
        this._basePath = _basePath;
    }

    public void setOwnerName(String inOwnerName) {
        this._ownerName = inOwnerName;
    }

    public String getPath() {
        return this._basePath + this._ownerName;
    }
}
