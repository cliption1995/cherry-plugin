package com.cliption.cherry.models.config;

public enum ConfigPath {
    COMMAND_BACKPACK_SLOTS("command.backpack.slots");

    private String _path;

    ConfigPath(String inPath) {
        _path = inPath;
    }

    public String getPath() {
        return _path;
    }
}
