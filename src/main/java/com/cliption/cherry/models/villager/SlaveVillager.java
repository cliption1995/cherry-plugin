package com.cliption.cherry.models.villager;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.UUID;

public class SlaveVillager {
    public Villager _villager;

    public SlaveVillager(Villager inVillager) {
        _villager = inVillager;
        if (_villager != null) {
            changeCharacterDesign();
            changeDetails();
        } else {
            // Do nothing.. shouldn't be happening through...
        }
    }

    private void changeCharacterDesign() {
        EntityEquipment equipment = _villager.getEquipment();
        if (equipment != null) {
            equipment.setHelmet(createAndReturnItemStack(Material.LEATHER_HELMET));
            equipment.setChestplate(createAndReturnItemStack(Material.LEATHER_CHESTPLATE));
            equipment.setLeggings(createAndReturnItemStack(Material.LEATHER_LEGGINGS));
            equipment.setBoots(createAndReturnItemStack(Material.LEATHER_BOOTS));
            _villager.getEquipment().setArmorContents(equipment.getArmorContents());
        } else {
            // Not good... DAMN!
        }
    }

    private void changeDetails() {
        _villager.setCustomName(UUID.randomUUID().toString());
        //_villager.setCustomNameVisible(true); // Name will be always visible. It is visible if you aim at him (default)

        _villager.setCanPickupItems(true);
        _villager.setHealth(5); // Research, if is's too low

        _villager.setProfession(Villager.Profession.SHEPHERD);
    }

    private ItemStack createAndReturnItemStack(Material inMaterial) {
        ItemStack leatherArmorPiece = new ItemStack(inMaterial);
        LeatherArmorMeta itemMeta = (LeatherArmorMeta) leatherArmorPiece.getItemMeta();
        itemMeta.setColor(Color.BLACK);
        leatherArmorPiece.setItemMeta(itemMeta);
        return leatherArmorPiece;
    }

}
